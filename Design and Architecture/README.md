## Software Design and Architecture Fundamentals

- [Learning materials](https://gitlab.com/procoders.school/js-hub/-/blob/master/Design%20and%20Architecture/materials.md)

- [Evaluation Framework](https://gitlab.com/procoders.school/js-hub/-/blob/master/Design%20and%20Architecture/evaluation.md)
