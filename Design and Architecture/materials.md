# Learning materials

1. Books:

    - [Learning JavaScript Design Patterns](https://addyosmani.com/resources/essentialjsdesignpatterns/book/) by Addy Osmani

    - [Солидбук](https://ota-solid.vercel.app/)

    - [The Pragmatic Programmer: From Journeyman to Master](https://www.amazon.com/Pragmatic-Programmer-Journeyman-Master/dp/020161622X) by Andrew Hunt

    - [Design Patterns: Elements of Reusable Object-Oriented Software](https://www.amazon.com/Design-Patterns-Object-Oriented-Addison-Wesley-Professional-ebook/dp/B000SEIBB8)

    - [Clean CodeClean Code: A Handbook of Agile Software Craftsmanship](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882) by Robert Martin

    - [Clean Architecture: A Craftsman's Guide to Software Structure and Design](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164) by Robert Martin

    - [Domain-Driven Design: Tackling Complexity in the Heart of Software](https://www.amazon.com/Domain-Driven-Design-Tackling-Complexity-Software/dp/0321125215) by Eric Evans

    - [Patterns of Enterprise Application Architecture](https://martinfowler.com/books/eaa.html) by Martin Fowler

    - [Refactoring: Improving the Design of Existing Code](https://martinfowler.com/books/refactoring.html) by Martin Fowler
    
    - [Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions](https://www.amazon.com/o/asin/0321200683/ref=nosim/enterpriseint-20) by Gregor Hohpe


2. Articles:

    - [Domain driven design](https://khalilstemmler.com/articles/categories/domain-driven-design/) by Khalil Stemmler

    - [Software Architecture Guide](https://martinfowler.com/architecture/)

    - [Client-Side Architecture Basics](https://khalilstemmler.com/articles/client-side-architecture/introduction/) by Khalil Stemmler

    - [The Twelve-Factor App](https://12factor.net/)

3. Talks
    
    [in progress]

4. Lectures

    - [GRASP](https://www.youtube.com/playlist?list=PLHhi8ymDMrQby8kXxsz2-J6-lsv0ilEg2) (Тимур Шемсединов)
    
    - [Архитектура программных систем](https://www.youtube.com/playlist?list=PLHhi8ymDMrQYGZLuEc92Sp0uO2fhoSslz) (Тимур Шемсединов)





