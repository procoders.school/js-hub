We know that you mastered the fundamentals of React if:

- You can write reusable react components

- You can structure complex react components

- You know and understand react component’s lifecycle

- You understand how all the standard React hooks work

- You know how to use context

- You can write your own reusable hooks

- You know how data fetching works in React

- You know how to manage state

- You can build simple react applications with a provided backend

