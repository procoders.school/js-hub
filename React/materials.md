# Learning materials

1. Required reading:

    - [React Documentation](https://reactjs.org/docs/getting-started.html) (all of it)

    - [Under-the-hood of React Hooks](https://indepth.dev/posts/1220/under-the-hood-of-react-hooks) by Craig Taub

    - [Inside Fiber: in-depth overview of the new reconciliation algorithm in React](https://indepth.dev/posts/1008/inside-fiber-in-depth-overview-of-the-new-reconciliation-algorithm-in-react) by Max Koretskyi

    - [In-depth explanation of state and props update in React](https://indepth.dev/posts/1009/in-depth-explanation-of-state-and-props-update-in-react) by Max Koretskyi

    - [The how and why on React’s usage of linked list in Fiber to walk the component’s tree](https://indepth.dev/posts/1007/the-how-and-why-on-reacts-usage-of-linked-list-in-fiber-to-walk-the-components-tree) by Max Koretskyi

    - [React is slow, what now?](https://nosleepjavascript.com/react-performance/)

    - [Kent Dodds](https://kentcdodds.com/blog/) blog

    - [Overreacted blog](https://overreacted.io/) by Dan Abramov

    - [The Complete Redux book](https://leanpub.com/redux-book) by Ilya Gelman and Boris Dinkevich

    - [Thinking in Redux](https://www.goodreads.com/book/show/40848858-thinking-in-redux) by Nir Kaufman

2. Talks:

    [in progress]

3. Workshops:

    - [React Fiber Deep Dive with Dan Abramov](https://www.youtube.com/watch?v=aS41Y_eyNrU&list=WL&index=4&ab_channel=SmooshComedy)

    - [Advanced React patterns](https://github.com/kentcdodds/advanced-react-patterns) by Kent Dodds

    - [Advanced React hooks](https://github.com/kentcdodds/advanced-react-hooks) by Kent Dodds

    - [канал АйТи Синяк](https://www.youtube.com/channel/UClgj-KWiNaOo9H1rz1ISO6Q/videos)


