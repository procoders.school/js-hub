# Learning materials

1. Required reading:

    - [Node Documentation](https://nodejs.org/en/docs/)

    - [Node.js Best Practices](https://github.com/goldbergyoni/nodebestpractices)

    - [Tao of Node](https://alexkondov.com/tao-of-node/)
    
2. Talks

    - [Антипаттерны Node.js](https://www.youtube.com/watch?v=DJCzZF383ug&ab_channel=FestGroup) (Тимур Шемсединов)

    - [Эффективная разработка NodeJS приложений](https://www.youtube.com/watch?v=g0I2dNwK1BE&list=WL&index=2&t=4872s&ab_channel=fwdays)

    - [Реализация гексагональной архитектуры на node.js и nest](https://www.youtube.com/watch?v=sMWwO9yRrCc&ab_channel=devschacht) (Андрей Мелихов)

    - [Архитектура современного корпоративного Node js приложения](https://www.youtube.com/watch?v=dQjXIuaq-yo) (Андрей Мелихов)

    - [Разделяемая память в многопоточном Node.js](https://www.youtube.com/watch?v=KNsm_iIQt7U&ab_channel=FestGroup) (Тимур Шемсединов)

3. Lectures

    - [Node.js – Введение в технологию](https://www.youtube.com/playlist?list=PLHhi8ymDMrQZmXEqIIlq2S9-Ibh9b_-rQ) (Тимур Шемсединов) 

    - [Асинхронное программирование](https://www.youtube.com/playlist?list=PLHhi8ymDMrQZ0MpTsmi54OkjTbo0cjU1T) (Тимур Шемсединов)

    - [Node.js - Advanced Concepts](https://www.youtube.com/playlist?list=PLKu4a5OPnvu6-u8-w7D_QAQg2FmCCvC5-)

    - [Образовательная программа Metarhia](https://github.com/HowProgrammingWorks/Index)


4. [Node.js Reference architecture from Red Hat and IBM](https://nodeshift.dev/nodejs-reference-architecture)
