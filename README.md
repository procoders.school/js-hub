# Foreword

The main idea of this set of materials is getting you started with your own career path to become a professional JavaScript engineer.

Books, articles and talks presented in this repository are particularly good and useful for getting you familiar with the topics.
You are strongly recommended to read all the materials in English even if there exist community translations to your native language.

This list of materials is not exhaustive in any way, hence you can (_and should_) look for more articles and watch more talks as you become a more capable and experienced software engineer.


## Table of Contents
1. [JavaScript Fundamentals](https://gitlab.com/procoders.school/js-hub/-/tree/master/Fundamentals)

2. [React](https://gitlab.com/procoders.school/js-hub/-/tree/master/React)

3. [Node](https://gitlab.com/procoders.school/js-hub/-/tree/master/Node)

4. [Software Design and Architecture](https://gitlab.com/procoders.school/js-hub/-/tree/master/Design%20and%20Architecture)

