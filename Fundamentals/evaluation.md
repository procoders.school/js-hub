We know that you mastered JavaScript fundamentals if:

- You know how and when to use most of the JavaScript language constucts and syntax;

- You are able to write structured JavaScript programs and build generic reusable entities like functions and classes;

- You know (at least aware of) most of the array and string methods;

- You are comfortable with Promises, XHR, higher-order functions, DOM API, all the JS data types;

- You have basic understanding of the event loop;
