# Learning materials

1. Books:

    - [Eloquent JavaScript](https://eloquentjavascript.net/) by Marijn Haverbeke

    - You don’t know JS (all books) by Kyle Simpson

        - [English version](https://github.com/getify/You-Dont-Know-JS) (strongly recommended)
        - [Russian version](https://github.com/azat-io/you-dont-know-js-ru)

    - [Functional-Light JavaScript](https://github.com/getify/Functional-Light-JS) by Kyle Simpson

2. Articles:
    
    - [Understanding CORS](https://medium.com/@baphemot/understanding-cors-18ad6b478e2b)

    - [Clean code tips](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29)

    - [Memory management in V8](https://deepu.tech/memory-management-in-v8/)

    - [Garbage Collection in V8](https://v8.dev/blog/trash-talk)

    - [Память в браузерах и в Node.js: ограничения, утечки и нестандартные оптимизации](https://habr.com/ru/company/yandex/blog/666870/)


3. Talks
    
    - [What the heck is the event loop anyway?](https://www.youtube.com/watch?v=8aGhZQkoFbQ&ab_channel=JSConf) by Philip Roberts

    - [V8 под капотом](https://www.youtube.com/watch?v=SNs61SwZbTI&ab_channel=HolyJS) (Андрей Мелихов)


4. Lectures

    - [Образовательная программа Metarhia](https://github.com/HowProgrammingWorks/Index)     
