## JavaScript Fundamentals

- [Learning materials](https://gitlab.com/procoders.school/js-hub/-/blob/master/Fundamentals/materials.md)

- [Evaluation Framework](https://gitlab.com/procoders.school/js-hub/-/blob/master/Fundamentals/evaluation.md)
